# README #

### What is this repository for? ###

This is a static website that allows the user to generate graphs that help visualize statistics related to D&D 5e.

There are 3 main types of statistics that are supported:

* Doing damage against a creature (i.e. either AC attacks or spells)
* Saving throws/ability checks/group checks
* Just plain dice

I consider the website to be a version of [https://anydice.com/](https://anydice.com/) that is more specialized for D&D 5e.

It does not support the generic programming features that AnyDice has (variables, loops, if-statements, etc...) but does automatically handle many D&D 5e specific concepts that can be quite tricky to implement correctly (critical hits/misses, once-per-turn damage like Sneak Attack, etc...). For example, [this article](https://anydice.com/articles/dnd4-attacks/) explains how it is possible to compute these stats in AnyDice but it is quite involved comparatively.

### What does it look like? ###

[https://stuart-rudderham.bitbucket.io/dnd5e-statistics/](https://stuart-rudderham.bitbucket.io/dnd5e-statistics/)

![](./screenshot.png "Screenshot")

### Supported platforms ###

The website has been tested on the following desktop browsers:

* Chrome 83 (Win64)
* Microsoft Edge 83 (Win64)
* Firefox 77 (Win64)

The website is technically functional on mobile browsers but it was not designed for them. It has been tested on:

* Chrome 83 (Nexus 5, Android 6.0.1)
* Firefox 68 (Nexus 5, Android 6.0.1)

### Implementation details ###

All the calculations are performed client-side using these libraries

* [https://bitbucket.org/Stuart-Rudderham/d-d-5e-statistics](https://bitbucket.org/Stuart-Rudderham/d-d-5e-statistics)
* [https://bitbucket.org/Stuart-Rudderham/d-d-5e-statistics-webassembly-bridge](https://bitbucket.org/Stuart-Rudderham/d-d-5e-statistics-webassembly-bridge)


