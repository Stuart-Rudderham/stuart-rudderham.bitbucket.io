"use strict";

//
// Helper function to streamline creating new HTML elements.
//
function CreateAndAppendElement(elementToAppendTo, newElementName, newElementId)
{
    const newElement = document.createElement(newElementName);

    // Setting the ID is optional.
    if( newElementId )
    {
        newElement.id = newElementId;
    }

    elementToAppendTo.appendChild(newElement);

    return newElement;
}

//
// Helper function to redraw all the graphs based on the current use input.
//
function TriggerRedrawGraphs( inputValidationFunction, plottingObjectsList )
{
    const userInputTextArea = document.getElementById("user-input-text-area");
    const graphsSection     = document.getElementById("graphs-section");
    const errorsSection     = document.getElementById("errors-section");

    // Hide both sections by default. We don't yet
    // know which one we will end up showing.
    graphsSection.style.display = "none";
    errorsSection.style.display = "none";

    const GetUserInputValue = function()
    {
        // It is critical that we re-get the input from the element each
        // time. If we stored it in a separate variable it would be
        // captured by the closure and we'd always use the stale value.
        return userInputTextArea.value;
    };

    // We only draw the graphs if there is some input. The most common case
    // of having no input is when the the "Custom" example is first selected.
    if( GetUserInputValue() != "")
    {
        try
        {
            //
            // We want to validate the input before drawing any of
            // the graphs otherwise the WebAssembly might throw
            // an exception in the middle and we'll be left in an
            // indeterminate state.
            //
            // If the input is invalid this function will throw an
            // exception which will be displayed in the error section.
            //
            inputValidationFunction( GetUserInputValue() );

            // Input validation passes, we're good to show this
            // section now since there should be no future errors.
            graphsSection.style.display = "block";

            // Draw all the graphs.
            plottingObjectsList.forEach( (plottingObject, index) =>
            {
                const graphElementId = `zzGraph_${index}`;

                //
                // Always draw the graph. This has two benefits:
                //
                //     1) The graphs always appear in the same order on the page
                //
                //     2) It makes the show/hide logic below simpler since we don't
                //        have to worry about the graph element maybe not existing.
                //
                plottingObject.drawGraphFn( graphsSection, graphElementId, GetUserInputValue );

                // Some graphs are special and have extra input validation
                // that control if we show or hide the graph.
                if( plottingObject.shouldShowGraphFn )
                {
                    const graphElement = document.getElementById(graphElementId);
                    const shouldShowGraph = plottingObject.shouldShowGraphFn( GetUserInputValue() );

                    graphElement.style.display = shouldShowGraph ? "block" : "none";
                }
            });
        }
        catch(err)
        {
            const splitInputText = err.inputText.split("\n");

            // Error reporting currently doesn't include the line/column for the error
            // If this changes in the future you can uncomment the below lines of code.
            /*
            // The line for the parsing error is 1-indexed hence the "- 1"s.
            const lineBeforeError = splitInputText[err.line - 1].substring(0, err.column);
            const lineAfterError  = splitInputText[err.line - 1].substring(err.column);

            // Make it obvious where the error occurred (both line and column).
            splitInputText[err.line - 1] = `<span id="error-line">${lineBeforeError}</span>${lineAfterError}`;
            */

            const rejoinedInputText = splitInputText.join("\n");

            errorsSection.innerHTML = `
                <h2 id="syntax-error-header">Syntax Error:</h2>
                <br>
                <pre>${err.errorMessage}</pre>
                <br>
                <h2>Input:</h2>
                <br>
                <pre>${rejoinedInputText}</pre>
            `;

            // Show the error section.
            errorsSection.style.display = "block";
        }
    }
};

// Helper function to set the value of the example selector and trigger a graph redraw.
function SelectExampleByNameAndTriggerRedrawGraphs( exampleName )
{
    const exampleInputSelect = document.getElementById("example-input-select");

    const exampleOptions = document.querySelectorAll("#example-input-select > option");

    for( const exampleOption of exampleOptions )
    {
        if( exampleOption.text == exampleName )
        {
            // Set the selector value.
            exampleInputSelect.value = exampleOption.value;

            // Pretend the user set it themselves so
            // that all the normal effects trigger.
            const event = new Event("change");
            exampleInputSelect.dispatchEvent(event);

            break;
        }
    }
};

// Helper function to change the text we use for a given example.
// NOTE: This does not automatically trigger a graph redraw.
function UpdateExampleValue( exampleName, newExampleValue )
{
    const exampleOptions = document.querySelectorAll("#example-input-select > option");

    for( const exampleOption of exampleOptions )
    {
        if( exampleOption.text == exampleName )
        {
            exampleOption.value = newExampleValue;
            break;
        }
    }
};

//
// The main function that each webpage calls to setup everything.
//
async function SetupPage( pageDescription
                        , examplesList
                        , inputValidationFunction
                        , randomExampleGenerationFunction
                        , plottingFunctionsList )
{
    console.group("SetupPage");

    const totalTimeProfileTag = "total time to setup the page";
    console.time(totalTimeProfileTag);

    // Page Title.
    document.title = "D&D 5e Statistics";

    // Favicon.
    const favicon = CreateAndAppendElement(document.head, "link");
    favicon.rel = "icon"
    favicon.href = "favicon.ico";

    // Nav Bar.
    CreateNavBar(document.body);

    // Everything is a child of this.
    const mainContent = CreateAndAppendElement(document.body, "div", "main-content");

    // Page description.
    const pageDescriptionParagraph = CreateAndAppendElement(mainContent, "p");
    pageDescriptionParagraph.innerHTML = `<b>Input Data</b>: ${pageDescription}`;

    const examplesAndInputDiv = CreateAndAppendElement(mainContent, "div");

    const examplesDiv = CreateAndAppendElement(examplesAndInputDiv, "div", "examples-section");

    const examplesSelectorDiv = CreateAndAppendElement(examplesDiv, "div");

    // Example input selector.
    const importedExample = new URLSearchParams(window.location.search).get("imported-example");

    const exampleInputSelectLabel = CreateAndAppendElement(examplesSelectorDiv, "label", "example-input-select-label");
    exampleInputSelectLabel.for = "example-input-select-label";
    exampleInputSelectLabel.textContent = "Example: ";

    const exampleInputSelect = CreateAndAppendElement(examplesSelectorDiv, "select", "example-input-select");
    {
        // Give every page an empty example, just in case the user
        // doesn't realize that they can edit the actual examples.
        examplesList.push( {name: "Custom", value: ""} );

        //
        // Give every page an random example, so that the user can play
        // around an experiment with options they might not know exist.
        //
        // We have to delay generating the example until InstantiateWASM
        // is called, so use a placeholder value for now.
        //
        examplesList.push( {name: "Random", value: ""} );

        // Only if the user is using a link with an imported example.
        if( importedExample )
        {
            // Insert it at the front so that it's automatically the default example.
            examplesList.unshift( {name: "Imported", value: importedExample} );
        }

        examplesList.forEach(example =>
        {
            const option = CreateAndAppendElement(exampleInputSelect, "option");

            option.text = example.name;
            option.value = example.value;
        });
    }

    // Generate link.
    const randomExampleButton = CreateAndAppendElement(examplesDiv, "button", "random-example-button");
    {
        const randomExampleButtonIcon = CreateAndAppendElement(randomExampleButton, "div", "random-example-button-icon");

        //randomExampleButton.textContent = "Randomize";
        randomExampleButton.onclick = () =>
        {
            // Generate a new random example on each button press.
            UpdateExampleValue( "Random", randomExampleGenerationFunction() );

            SelectExampleByNameAndTriggerRedrawGraphs("Random");
        };
    }

    // Setup the text area where the user can enter their data.
    const userInputTextArea = CreateAndAppendElement(examplesAndInputDiv, "textarea", "user-input-text-area");
    {
        userInputTextArea.rows       = 8;
        userInputTextArea.spellcheck = false;
        userInputTextArea.wrap       = "off"; // Add a horizontal scroll bar rather than wrapping text.
    }

    // Generate link.
    const generateLinkButton = CreateAndAppendElement(examplesAndInputDiv, "button", "generate-link-button");
    {
        generateLinkButton.textContent = "Click To Generate Link";
        generateLinkButton.onclick = () =>
        {
            // Initialize link with current URL.
            const generatedURL = new URL(window.location.href);

            // Append current user input.
            generatedURL.searchParams.set("imported-example", userInputTextArea.value);

            generateLinkButton.textContent = "...";

            // https://stackoverflow.com/a/52174349
            let delay = new Promise(resolve => setTimeout(resolve, 250));

            // We want to copy the generated URL to the user's clipboard and notify
            // them that it has been copied.
            //
            // We delay so that the user has time to see the button text change to
            // "..." first, letting them know that their request has been processed.
            // This is not necessary for the first time the button is pressed but
            // otherwise the subsequent button presses wouldn't change the text and
            // the user would think that nothing has happened.
            delay.then (() => navigator.clipboard.writeText(generatedURL.href))
                 .then (() => generateLinkButton.textContent = "Copied To Clipboard!")
                 .catch(() => generateLinkButton.textContent = "Link Generation Failed :(");
        };
    }

    // Redraw graph.
    const redrawGraphsButton = CreateAndAppendElement(examplesAndInputDiv, "button", "redraw-graphs-button");
    {
        redrawGraphsButton.textContent = "Draw Graphs";
        redrawGraphsButton.onclick = () => TriggerRedrawGraphs(inputValidationFunction, plottingFunctionsList);
    }

    // Sections for content.
    const errorsSection = CreateAndAppendElement(mainContent, "div", "errors-section");
    const graphsSection = CreateAndAppendElement(mainContent, "div", "graphs-section");

    // Repo links.
    const repoLinksSection = CreateAndAppendElement(mainContent, "div");
    {
        const AddRepoLink = function(textContent, href)
        {
            const s = CreateAndAppendElement(repoLinksSection, "div");
            const repoLink = CreateAndAppendElement(s, "a");

            repoLink.href        = href;
            repoLink.target      = "_blank" // Open in a new tab always.
            repoLink.textContent = textContent;
        };

        AddRepoLink( "Website Source Code"    , "https://bitbucket.org/Stuart-Rudderham/stuart-rudderham.bitbucket.io/src/master/dnd5e-damage-statistics/" );
        AddRepoLink( "Rust Source Code"       , "https://bitbucket.org/Stuart-Rudderham/d-d-5e-statistics/"                                                );
        AddRepoLink( "WebAssembly Source Code", "https://bitbucket.org/Stuart-Rudderham/d-d-5e-statistics-webassembly-bridge/"                             );
    }

    //
    // Must be called before any of the other plotting functions.
    //
    // We want to do this as late in this function as possible to
    // get as much of the page setup as we can before blocking here.
    //
    // This makes it feel like the page loads faster to the user.
    //
    await InstantiateWASM();

    // Generate our first random example.
    UpdateExampleValue( "Random", randomExampleGenerationFunction() );

    // Need to do this after everything has been setup, since it
    // will reference/modify other elements in the document.
    {
        // When the user picks a different option from the
        // list update the text area and redraw the graph.
        exampleInputSelect.addEventListener("change", event =>
        {
            // Change the text to the example.
            userInputTextArea.value = event.target.value;

            // Automatically redraw the graphs.
            redrawGraphsButton.click();
        });

        // Explicitly pick an example so that the graphs automatically draw.
        SelectExampleByNameAndTriggerRedrawGraphs( examplesList[0].name );
    }

    console.timeEnd(totalTimeProfileTag);
    console.groupEnd();
}

function CreateNavBar(rootElement)
{
    const navBar = CreateAndAppendElement(rootElement, "nav", "nav-bar");

    const pages =
    [
        { name: "Damage Against Creature", link: "damage-against-creature.html" },
        { name: "Check Against DC"       , link: "check-against-dc.html"        },
        { name: "Dice"                   , link: "dice.html"                    },
    ];

    // Get the file name of the current page.
    // https://befused.com/javascript/get-filename-url
    var url = window.location.pathname;
    var filename = url.substring( url.lastIndexOf('/') + 1 );

    pages.forEach(example =>
    {
        const link = CreateAndAppendElement(navBar, "a");

        link.href = `./${example.link}`;
        link.textContent = example.name;

        if( example.link == filename )
        {
            link.className  = "current-page";
        }
        else
        {
            link.className  = "not-current-page";
        }
    });
}
